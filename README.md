# Towerback/Huebris #

An in-progress tile matching game made with Swift and SpriteKit. Designed for iPhone 6s.

Tap to place the tiles inside the tower blocks. Match three to clear tiles. Make matches to clear tower blocks. Try to stop the tower from getting too high.