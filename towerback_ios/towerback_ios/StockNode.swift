//
//  StockNode.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/6/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import SpriteKit

class StockNode: SKNode
{
/// Defines the diplay and fill direction of the Meter
    enum MeterDirection
    {
///     The meter's origin is at the bottom, and it fills from the bottom up
        case up
///     The meter's origin is at the top, and it fills from the top down
        case down
///     The meter's origin is on the right, and it fills from the right to the left
        case left
///     The meter's origin is on the left, and it fills from the left to the right
        case right
    }
    var direction : MeterDirection = .right
    
    var chargeTotal : Int = 0
    {
        didSet
        {
//          Remove excess ticks
            while self.tickNodes.count > self.chargeTotal
            {
                let nextTick = self.tickNodes.popLast()
                nextTick?.removeFromParent()
            }
//          Add new ticks
            while self.tickNodes.count < self.chargeTotal
            {
                let newTick = SKSpriteNode(imageNamed: self.tickName )
                self.tickNodes.append(newTick)
                self.addChild(newTick)
            }
            self.layoutTicks()
        }
    }
    var currentCharge : Int = 0
    
//    var anchorPoint : CGPoint = CGPoint(x: 0.5, y: 0.5)
    
    var tickName : String = "charge-empty"
    var tickNodes = [SKSpriteNode]()
    
    func addGem(_ value: Int)
    {
        for nextNodeIndex in 0..<self.tickNodes.count
        {
            let nextTick = self.tickNodes[nextNodeIndex]
            guard nextTick.childNode(withName: "fill") == nil else { continue }
            let fillNode = SKSpriteNode(imageNamed: "charge-fill-\(value)")
            fillNode.name = "fill"
            fillNode.zPosition = 3.0
            nextTick.addChild(fillNode)
            fillNode.run(self.tickLightUpAction(), withKey: "light_up")
            break
        }
    }
    
    
    fileprivate func layoutTicks()
    {
        var nextXPosition : CGFloat = 0.0
        var nextYPosition : CGFloat = 0.0
        for nextNode in tickNodes
        {
            nextNode.position.x = nextXPosition
            nextNode.position.y = nextYPosition
            var xOffset : CGFloat = 0.0
            var yOffset : CGFloat = 0.0
            switch self.direction
            {
            case .left:
                xOffset = -nextNode.frame.width
            case .right:
                xOffset = nextNode.frame.width
            case .down:
                yOffset = -nextNode.frame.height
            case .up:
                yOffset = nextNode.frame.height
            }
            nextXPosition += xOffset
            nextYPosition += yOffset
        }
        self.position.x = -self.calculateAccumulatedFrame().width / 2.0
    }
    
    fileprivate func tickLightUpAction() -> SKAction
    {
        let blinkAction = SKAction.colorize(with: .white, colorBlendFactor: 1.0, duration: 0.13)
        let lightAction = SKAction.colorize(with: .clear, colorBlendFactor: 0.0, duration: 0.13)
        return SKAction.sequence([blinkAction, lightAction])
    }
}
