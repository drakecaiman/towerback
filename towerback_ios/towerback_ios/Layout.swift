//
//  Layout.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/23/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

class Layout
{
    enum DistributeLengthError : Error
    {
        case invalidCount
    }
    static func distributeLength(_ length: Float, between count: Int, withPadding padding: Float = 0.0) throws -> Float
    {
        guard count > 0 else { throw DistributeLengthError.invalidCount }
        let totalPadding =  padding * Float(count - 1)
        return (length - totalPadding) / Float(count)
    }
}
