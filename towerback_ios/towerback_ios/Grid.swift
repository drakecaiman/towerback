//
//  Grid.swift
//  bitbyter
//
//  Created by Duncan Oliver on 10/23/15.
//  Copyright © 2015 Highjacks. All rights reserved.
//

/**
 Grid objects are structures used to store values in a multi-dimensional manner. Indices are defined along x and y axes as integers, with each having an optional range for index values.
 */
public struct Grid<StoredType>: CustomStringConvertible
{
/// The dictionary used to store the grid's values
    fileprivate var map = [GridIndex: StoredType]()
/// The upper- and lower-bound limits for x values of this grid's index
//    let xRange: Range<Int>?
/// The upper- and lower-bound limits for y values of this grid's index
//    let yRange: Range<Int>?
    let ranges: [CountableRange<Int>?]?
    
    public init(withRanges ranges: CountableRange<Int>?...)
    {
        self.ranges = ranges
    }

//  TODO: Organize map by index before output
    public var description: String
    {
        return map.description
    }
    
    subscript(indexes: Int...) -> StoredType?
    {
        get
        {
            if ranges != nil
            {
    //          Check ranges
                for (nextPosition, nextIndex) in indexes.enumerated()
                {
                    guard let nextRange = ranges![nextPosition] else { continue }
                    guard nextRange ~= nextIndex else { return nil }
                }
            }
            
            return map[GridIndex(indexes)]
        }
        set
        {
            if ranges != nil
            {
    //          Check ranges
                for (nextPosition, nextIndex) in indexes.enumerated()
                {
                    guard let nextRange = ranges![nextPosition] else { continue }
                    guard nextRange ~= nextIndex else { return }
                }
            }
            
            map[GridIndex(indexes)] = newValue
        }

    }
    
    subscript(indexes: CountableRange<Int>...) -> [StoredType]?
    {
        get
        {
            if ranges != nil
            {
//              Check ranges
                for (nextPosition, nextIndex) in indexes.enumerated()
                {
                    guard let nextRange = ranges![nextPosition] else { continue }
                    guard nextRange.lowerBound <= nextIndex.lowerBound &&
                        nextRange.upperBound >= nextIndex.upperBound
                    else { return nil }
                }
            }
            
            var gridIndexes = [GridIndex]()
            for nextIndexRange in indexes
            {
                var resultingIndexes = [GridIndex]()
                if gridIndexes.count < 1
                {
                    resultingIndexes = nextIndexRange.map { GridIndex([$0]) }
                }
                else
                {
                    let existingIndexes = gridIndexes
                    for nextExistingIndex in existingIndexes
                    {
                        for nextNewIndex in nextIndexRange
                        {
                            var newResultIndex = nextExistingIndex
                            newResultIndex.indexes.append(nextNewIndex)
                            resultingIndexes.append(newResultIndex)
                        }
                    }
                }
                
                gridIndexes = resultingIndexes
            }
            
            var result = [StoredType]()
            for nextIndex in gridIndexes
            {
                guard let nextItem = map[nextIndex] else { continue }
                result.append(nextItem)
            }
            
            return result
        }
    }
}

struct GridIndex : Hashable
{
    var indexes : [Int]
    
    init(_ indexes: [Int])
    {
        self.indexes = indexes
    }
    
    var hashValue: Int
    {
        return indexes.reduce(0) {$0 ^ $1}
    }
}

func == (lhs: GridIndex, rhs: GridIndex) -> Bool
{
    guard lhs.indexes.count == rhs.indexes.count else { return false }
    for (nextPosition, nextLeftHandIndex) in lhs.indexes.enumerated()
    {
        let nextRightHandIndex = rhs.indexes[nextPosition]
        if nextLeftHandIndex != nextRightHandIndex { return false }
    }
    
    return true
}

/**
 GridError objects stem from accessing or assigning values in Grid objects.
 */
enum GridError: Error
{
/// When throwing a OutOfRange error, this enumeration is used to describe the axis the range belongs to.
    enum GridAxis
    {
///     This case is used to describe the x-axis
        case x
///     This case is used to describe the y-axis
        case y
    }

    /**
     This error is thrown when the index used to get or set an object is not in range for the given axis.

     - parameters:
        - axis:
        The axis, x or y, of the unsatisfied range
        - allowedRange:
        The expected range for the index
     */
    case outOfRange(position: Int, allowedRange: CountableRange<Int>)
}
