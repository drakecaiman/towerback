//
//  TowerCategoryOptionSet
//  towerback_ios
//
//  Created by Duncan Oliver on 8/18/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

/**
 `TowerCategoryOptionSet` is used to defined category bitmasks for physics bodies in `TowerScene` scenes.
 */
struct TowerCategoryOptionSet : OptionSet
{
/// The underlying value of this option set
    let rawValue : UInt32
    
/// The bitmask value of this option set
    var bitmask : UInt32
    {
        return self.rawValue
    }
    
//  MARK: Options
/// The option for game piece scenery
    static let environment  = TowerCategoryOptionSet(rawValue: 0b1 << 0)
/// The option for block boards
    static let block        = TowerCategoryOptionSet(rawValue: 0b1 << 1)
/// The option for game piece nodes
    static let piece        = TowerCategoryOptionSet(rawValue: 0b1 << 2)
/// The option for board spaces
    static let space        = TowerCategoryOptionSet(rawValue: 0b1 << 3)
/// The option for board spaces
    static let tower        = TowerCategoryOptionSet(rawValue: 0b1 << 4)
/// The option for UI elements
    static let ui           = TowerCategoryOptionSet(rawValue: 0b1 << 10)
    
//  MARK: Option sets
/// An option set for all currently defined tower scene categories
    static let AllDefined : TowerCategoryOptionSet = [.environment, .block, .piece, .space, .tower, .ui]
/// An option set for all categories
    static let All = TowerCategoryOptionSet(rawValue: UInt32.max)
}
