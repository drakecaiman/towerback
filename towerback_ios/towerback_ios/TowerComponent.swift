//
//  TowerComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/3/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import SpriteKit
import GameplayKit

class TowerComponent: GKComponent
{
    fileprivate let TOWER_COMPONENT_START_RUNE_COUNT_KEY            = "startRuneCount"
    fileprivate let TOWER_COMPONENT_MAX_RUNE_COUNT_KEY              = "maxRuneCount"
    fileprivate let TOWER_COMPONENT_START_MATCH_COUNT_KEY           = "startMatchCount"
    fileprivate let TOWER_COMPONENT_START_NEW_BLOCK_INTERVAL_KEY    = "startNewBlockInterval"
    fileprivate let TOWER_COMPONENT_MATCH_INCREASE_INTERVAL_KEY     = "matchIncreaseInterval"
    fileprivate let TOWER_COMPONENT_RUNE_INCREASE_INTERVAL_KEY      = "runeIncreaseInterval"
    fileprivate let TOWER_COMPONENT_MAX_BLOCK_HEIGHT_KEY            = "maxBlockHeight"
    
/// The current number of possible runes generated
    @GKInspectable var runeCount : Int = 1
    /// The max number of possible runes generated
    @GKInspectable var maxRuneCount : Int = 1
/// The current number of matches new blocks need to clear
    var maxMatches : Int = 1
/// The amount of time between spawning new blocks
    var newBlockInterval : TimeInterval = 60.0
/// The number of blocks to clear before the match count is increased
    var matchIncreaseCount : Int = 1
/// The number of blocks to clear before the rune count is increased
    var runeIncreaseCount : Int = 1
/// The max number of blocks in a tower before a game over
    var maxHeight : Int = 1
//  TODO: Remove reliance on block size
/// The size of a block
    var blockSize : CGSize?
    
    init(withParameters parameters: [String: AnyObject]? = nil)
    {
        if let unWrappedParameters = parameters
        {
            if let parameterRuneCount = unWrappedParameters[TOWER_COMPONENT_START_RUNE_COUNT_KEY] as? Int
            {
                self.runeCount = parameterRuneCount
            }
            if let parameterMaxRuneCount = unWrappedParameters[TOWER_COMPONENT_MAX_RUNE_COUNT_KEY] as? Int
            {
                self.maxRuneCount = parameterMaxRuneCount
            }
            if let parameterMatchCount = unWrappedParameters[TOWER_COMPONENT_START_MATCH_COUNT_KEY] as? Int
            {
                self.maxMatches = parameterMatchCount
            }
            if let parameterBlockInterval = unWrappedParameters[TOWER_COMPONENT_START_NEW_BLOCK_INTERVAL_KEY] as? TimeInterval
            {
                self.newBlockInterval = parameterBlockInterval
            }
            if let parameterMatchIncreaseInterval = unWrappedParameters[TOWER_COMPONENT_MATCH_INCREASE_INTERVAL_KEY] as? Int
            {
                self.matchIncreaseCount = parameterMatchIncreaseInterval
            }
            if let parameterRuneIncreaseInterval = unWrappedParameters[TOWER_COMPONENT_RUNE_INCREASE_INTERVAL_KEY] as? Int
            {
                self.runeIncreaseCount = parameterRuneIncreaseInterval
            }
            if let parameterMaxHeight = unWrappedParameters[TOWER_COMPONENT_MAX_BLOCK_HEIGHT_KEY] as? Int
            {
                self.maxHeight = parameterMaxHeight
            }
        }
        
        super.init()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
//        fatalError("init(coder:) has not been implemented")
    }
}
