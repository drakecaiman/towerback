//
//  RuleSystemComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/28/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit


class RuleSystemComponent: OwnerComponent<GKRuleSystem>
{
    convenience init()
    {
        self.init(with: GKRuleSystem())
    }
    
    override init(with content: GKRuleSystem)
    {
        super.init(with: content)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
//        fatalError("init(coder:) has not been implemented")
    }
}
