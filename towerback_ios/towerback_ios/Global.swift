//
//  Global.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/27/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import SpriteKit

enum SceneZPosition : CGFloat
{
    case pauseScene = 10.0
    case ui = 99.0
}
