//
//  BlockNode.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/16/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import SpriteKit

class BlockNode: SKSpriteNode
{
    let BLOCK_NODE_SPACE_NODE_NAME = "space"
    
    var spacePadding : CGFloat = 4.0
    override var size : CGSize
    {
        didSet
        {
            self.layout()
        }
    }
    override var centerRect: CGRect
        {
        didSet
        {
            self.updateSpaceNodes()
        }
    }
    var isActive : Bool = false
    {
        didSet
        {
    //      Set up block spaces
            self.enumerateChildNodes(withName: "\(BLOCK_NODE_SPACE_NODE_NAME).*.*")
            {
                node, stop in
                node.isUserInteractionEnabled = self.isActive
            }
        }
    }
    var spaceSize   : CGSize?
    {
        let availableSize = CGSize(width: self.centerRect.width * self.frame.width,
                                   height: self.centerRect.height * self.frame.height)
        guard let blockGrid = self.entity?.component(ofType: GridComponent.self)?.grid,
            let columnCount = blockGrid.ranges?[0]?.count,
            let rowCount = blockGrid.ranges?[1]?.count
        else
        {
            return nil
        }
        
        guard let spaceWidth = try? Layout.distributeLength(Float(availableSize.width), between: columnCount, withPadding: Float(self.spacePadding)),
            let spaceHeight = try? Layout.distributeLength(Float(availableSize.height), between: rowCount, withPadding: Float(self.spacePadding))
        else
        {
            return nil
        }
        
        return CGSize(width: CGFloat(spaceWidth), height: CGFloat(spaceHeight))
    }
    
    func getCenterRect() -> CGRect
    {
        let fullLength = 375.0
        let centerLength = 264.0
        let backgroundCenterSize    = CGSize(width: centerLength / fullLength, height: centerLength / fullLength)
        let centerOffset            = (fullLength - centerLength) / 2.0
        let backgroundCenterOrigin  = CGPoint(x: centerOffset / fullLength, y: centerOffset / fullLength)
        return CGRect(origin: backgroundCenterOrigin, size: backgroundCenterSize)
    }

    
    func layout()
    {
        self.centerRect = self.getCenterRect()
        if let meterNode = self.childNode(withName: "meter")
        {
            meterNode.position.x = -meterNode.calculateAccumulatedFrame().width / 2.0
        }
        self.updateSpaceNodes()
    }
    
    fileprivate func updateSpaceNodes()
    {
        guard let spaceSize = self.spaceSize else { return }
        
        var nextXPosition : CGFloat = -(self.centerRect.size.width * self.size.width) / 2.0 + spaceSize.width / 2.0
        var nextColumnIndex = 0
        while let _ = self.childNode(withName: "\(BLOCK_NODE_SPACE_NODE_NAME).\(nextColumnIndex).*") as? SpaceNode
        {
            var nextRowIndex = 0
            var nextYPosition : CGFloat = -(self.centerRect.size.height * self.size.height) / 2.0 + spaceSize.height / 2.0
            while let existingSpaceNode = self.childNode(withName: "\(BLOCK_NODE_SPACE_NODE_NAME).\(nextColumnIndex).\(nextRowIndex)") as? SpaceNode
            {
                existingSpaceNode.size      = spaceSize
                existingSpaceNode.position  = CGPoint(x: nextXPosition, y: nextYPosition)
//              TODO: Setting this to anything above 0.0 (lowest value tried 0.5) puts this in front of pause overlay (zPosition of 10.0). Why? (Possibly take parent zPosition and add it to set zPosition?)
                existingSpaceNode.zPosition = 1.0
//              TODO: Remove
                existingSpaceNode.addChild(SKShapeNode(rectOf: existingSpaceNode.size!))
                nextYPosition   += spaceSize.height + self.spacePadding
                nextRowIndex    += 1
            }
            nextXPosition   += spaceSize.width + self.spacePadding
            nextColumnIndex += 1
        }
    }
}

