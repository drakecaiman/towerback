//
//  GameViewController.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/16/16.
//  Copyright (c) 2016 Highjacks. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()

        if let scene = GKScene(fileNamed:"TitleScene")
        {
            // Configure the view.
            let skView = self.view as! GameSceneView
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = false
            
            (scene.rootNode as? TitleScene)?.gkScene = scene
            scene.reloadEntities()
            skView.presentScene(scene.rootNode as? SKScene)
        }
    }

    override var shouldAutorotate : Bool
    {
        return true
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask
    {
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            return .portrait
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden : Bool
    {
        return true
    }
}
