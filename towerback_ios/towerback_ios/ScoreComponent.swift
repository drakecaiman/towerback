//
//  ScoreComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/9/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class ScoreComponent: GKComponent
{
    fileprivate(set) var score = 0
    
    func increaseScore(by increaseValue: Int = 1)
    {
        self.score += increaseValue
    }
    
    func decreaseScore(by decreaseValue: Int = 1)
    {
        self.score -= decreaseValue
    }
}
