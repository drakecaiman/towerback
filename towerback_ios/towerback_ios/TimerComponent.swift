//
//  TimerComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/29/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

// TODO: delay before first firing?
// TODO: In Scene Editor, add a begin started parameter?
/**
 `TimerComponent` uses the update loop of a scene to trigger an action after a given amount of time. The timer can be set to fire repeatedly.
 */
@available(macOS 10.11, iOS 10.0, *)
class TimerComponent: GKComponent
{
    //  MARK: Properties
    /// The amount of time in seconds before execution of this component's action
    var interval                : TimeInterval = 0.0
    /// The time in seconds until the next firing of this component's action
    private(set) var timeLeft   : TimeInterval = 0.0
    /// Indicates whether or not the component
    @GKInspectable var isRepeating = false
    /// Indicates if this component is finished firing
    private(set) var isFinished = true
    /// The action to run at each interval of this component. The closure takes this `TimerComponent` as a parameter.
    /// - note: There is no way currently to set a closure as a value in the Scene Editor, so this must remain optional and variable to be usable in the Scene Editor. Ideally, this would be a constant set at initialization and non-optional.
    var action : ((_ timer: TimerComponent) -> ())?
    /// The rate at which this component's action fires per second
    @GKInspectable var rate : Float
        {
        get
        {
            return 1.0 / Float(self.interval)
        }
        set
        {
            self.interval = 1.0 / TimeInterval(newValue)
        }
    }
    
    //  MARK: Initializers
    //  TODO: Doc not showing up when @escaping is used
    /**
     Creates a new `TimerComponent` which will run its specified action at the given interval.
     
     - parameter interval:    The number of seconds to wait before executing this component's action
     - parameter isRepeating: Specifies whether the timer should restart its countdown after firing
     - parameter shouldStart: Specifies whether the timer should start running on creation
     - parameter action:      The action to execute at the end of this component's countdown
     - parameter timer:       The `TimerComponent` triggering this action
     
     - returns: A new `TimerComponent` with the specified interval
     */
    init(forInterval interval: TimeInterval, repeating isRepeating: Bool? = nil, started shouldStart: Bool? = nil, withAction action: @escaping (_ timer: TimerComponent) -> ())
    {
        self.interval = interval
        self.timeLeft = self.interval
        if let unwrappedIsRepeating = isRepeating
        {
            self.isRepeating = unwrappedIsRepeating
        }
        if let unwrappedShouldStart = shouldStart
        {
            self.isFinished = !unwrappedShouldStart
        }
        self.action = action
        
        super.init()
    }
    
    /// - note: When using with Scene Editor, an explicit `init()` method is required to prevent a fatal error. Ideally, this component should only be created programmatically for the time being as the action closure aspect is not representable in the Scene Editor.
    override init()
    {
        super.init()
    }
    
    
    /// - note: The action can not be archived for `NSCoding`, so any `TimerComponent` will have to have its action reapplied.
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    //  MARK: -
    /**
     Starts the countdown for this component. If used after calling `pause()`, it will continue its countdown from where it left off.
     */
    func start()
    {
        self.isFinished = false
    }
    
    /**
     Pauses the countdown for this component.
     */
    func pause()
    {
        self.isFinished = true
    }
    
    /**
     Stops the countdown for this component.
     */
    func stop()
    {
        self.isFinished = true
        self.timeLeft   = self.interval
    }
    
    /**
     Resets the timer's progress and begins it's countdown from the beginning.
     */
    func reset()
    {
        self.timeLeft   = self.interval
        self.isFinished = false
    }
    
    /**
     Fires this component's action
     */
    private func fire()
    {
        self.action?(self)
    }
    
    //  MARK: GKComponent methods
    override func update(deltaTime seconds: TimeInterval)
    {
        //      Decrement timer
        if !self.isFinished
        {
            self.timeLeft -= seconds
        }
        
        if self.timeLeft <= 0
        {
            //          Fire off action
            self.fire()
            //          Reset timer
            self.isFinished = !self.isRepeating
            self.timeLeft   = self.interval
        }
    }
}
