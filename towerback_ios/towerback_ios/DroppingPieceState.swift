//
//  DroppingPieceState.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/27/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit
import SpriteKit

class DroppingPieceState: ActionState
{
    override func isValidNextState(_ stateClass: AnyClass) -> Bool
    {
        switch stateClass
        {
        case is IdlePieceState.Type:
            return true
        default:
            return false
        }
    }
    
    override func didEnter(from previousState: GKState?)
    {
        self.triggerAction()
        {
            self.stateMachine?.enter(IdlePieceState.self)
        }
    }
}
