//
//  PieceComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/23/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class PieceComponent: GKComponent
{
    var space : GKEntity?
    
    func moveTo(_ newSpace: GKEntity)
    {
        self.space?.component(ofType: HoldComponent.self)?.piece = nil
        guard let destinationHoldComponent = newSpace.component(ofType: HoldComponent.self) else
        {
            return
        }
        destinationHoldComponent.piece = self.entity
        self.space = newSpace
    }
}
