//
//  GameScene.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/16/16.
//  Copyright (c) 2016 Highjacks. All rights reserved.
//

import SpriteKit
import GameplayKit

enum TowerSceneOverlay : String
{
    case pause      = "PauseScene"
    case gameOver   = "GameOverScene"
}

class TowerScene: SKScene
{
//  MARK: Constants
/// The node name for the UI 'layer'
    let TOWER_SCENE_NODE_NAME_UI                    = "ui"
/// The node name for the pause button
    let TOWER_SCENE_NODE_NAME_PAUSE_BUTTON          = "button.pause"
/// The node name for the next block timer display
    let TOWER_SCENE_NODE_NAME_UI_NEXT_BLOCK_TIMER   = "next_block_timer"
/// The node name for the tower height display
    let TOWER_SCENE_NODE_NAME_UI_HEIGHT_DISPLAY     = "height_display"
/// The node name for the next game piece display
    let TOWER_SCENE_NODE_NAME_STACK                 = "stack"
/// The node name for the platform blocks fall onto
    let TOWER_SCENE_NODE_NAME_PEDESTAL              = "pedestal"
/// The node name for the starting point of newly created blocks
    let TOWER_SCENE_NODE_NAME_BLOCK_START           = "block.start"
/// The node name for a block
    let TOWER_SCENE_NODE_NAME_BLOCK                 = "block"
/// The speed of the camera position movement
    let TOWER_SCENE_CAMERA_SPEED : CGFloat  = 175.0

//  MARK: Properties
/// The `GKScene` for this scene, used to manage GameplayKit objects
    var gkScene : GKScene?
/// The last time the `update(_:)` method was called for this scene
    private(set) var lastUpdateTime : TimeInterval?
/// The number of blocks that have been clear from the current scene
    var blockClearCount : Int = 0
/// The currently active block for this tower
    var activeBlockNode : BlockNode?
    {
        didSet
        {
            oldValue?.isActive = false
            activeBlockNode?.isActive = true
            self.lookAtActiveBlock()
        }
    }
/// The node used for displaying ui
    var uiNode : SKNode?
    {
        return self.childNode(withName: "//\(TOWER_SCENE_NODE_NAME_UI)")
    }
/// The node used for displaying ui
    var pauseButtonNode : SKNode?
    {
        return self.childNode(withName: "//\(TOWER_SCENE_NODE_NAME_PAUSE_BUTTON)")
    }
/// The node used for displaying the amount of time until the next block
    var nextBlockTimerNode : SKNode?
    {
        return self.childNode(withName: "//\(TOWER_SCENE_NODE_NAME_UI_NEXT_BLOCK_TIMER)")
    }
/// The node used for displaying the tower height
    var heightDisplayNode : SKNode?
    {
        return self.childNode(withName: "//\(TOWER_SCENE_NODE_NAME_UI_HEIGHT_DISPLAY)")
    }
/// The stack for this tower scene
    var stackNode : SKNode?
    {
        return self.childNode(withName: "//\(TOWER_SCENE_NODE_NAME_STACK)")
    }
/// The pedestal for this tower scene
    var pedestalNode : SKNode?
    {
        return self.childNode(withName: "//\(TOWER_SCENE_NODE_NAME_PEDESTAL)")
    }
/// The starting position for new blocks
    var blockStartNode : SKNode?
    {
        return self.childNode(withName: "//\(TOWER_SCENE_NODE_NAME_BLOCK_START)")
    }
/// The current number of block in the scene
    var currentBlockCount : Int
    {
        return self["//\(TOWER_SCENE_NODE_NAME_BLOCK)"].count
    }
    
//  MARK: -
    func dropNextPieceInto(_ node: SKNode)
    {
        guard let dropSpace = node.entity,
            dropSpace.component(ofType: HoldComponent.self)?.piece == nil
            else { return }
        guard let nextPiece = self.stackNode?.entity?.component(ofType: ReservoirComponent.self)?.nextPiece()
            else { return }
        self.entity?.component(ofType: DropperComponent.self)?.dropPiece(nextPiece, into: dropSpace)
    }
    
    func dropNewBlock()
    {
        let matches = self.entity?.component(ofType: TowerComponent.self)?.maxMatches ?? 1
//      Create the starting block
        let newBlock = Entities.newBlock(withNumberOfRows: 4, numberOfColumns: 4, matches: matches)
        self.gkScene?.addEntity(newBlock)
        if let blockNode = newBlock.component(ofType: GKSKNodeComponent.self)?.node as? BlockNode
        {
            blockNode.name = TOWER_SCENE_NODE_NAME_BLOCK
            blockNode.position  = self.blockStartNode?.position ?? CGPoint.zero
    //      Constraint block to only fall on pedestal
            blockNode.constraints = [SKConstraint]()
            blockNode.constraints?.append(SKConstraint.zRotation(SKRange(constantValue: 0.0)))
            blockNode.constraints?.append(SKConstraint.positionX(SKRange(constantValue: blockNode.position.x)))
//          Set up block node
            (newBlock.component(ofType: GKSKNodeComponent.self)?.node.childNode(withName: "meter") as? StockNode)?.chargeTotal = matches
            self.addChild(blockNode)
        }
    }
    
    func lookAtActiveBlock()
    {
        guard let towerCamera = self.camera,
            let currentBlockNode = self.activeBlockNode
            else { return }
        let panDistance = currentBlockNode.position.y - towerCamera.position.y
        let panDuration = TimeInterval(abs(panDistance) / TOWER_SCENE_CAMERA_SPEED)
        let panAction = SKAction.moveTo(y: currentBlockNode.position.y, duration: panDuration)
        panAction.timingMode = .easeInEaseOut
        towerCamera.run(panAction, withKey: "pan")
    }
    
    func increaseBlockCount()
    {
        if let towerRuleSystem = self.entity?.component(ofType: RuleSystemComponent.self)?.content
        {
            self.blockClearCount += 1
            towerRuleSystem.state["currentBlockClearCount"] = self.blockClearCount
        }
    }
    
    func togglePause()
    {
        guard let sceneState = self.entity?.component(ofType: StateMachineComponent.self)?.content.currentState else
        {
            print("No state machine found.")
            return
        }
        let nextState : AnyClass = sceneState is PauseState ? PlayingState.self : PauseState.self
        
        self.entity?.component(ofType: StateMachineComponent.self)?.content.enter(nextState)
    }
    
    func pauseGame()
    {
        self.isPaused = true
//      TODO: Should `lastUpdateTime` be nil, or should I find another way to update `lastUpdateTime` while paused?
        self.lastUpdateTime = nil
        self.addOverlay(ofType: .pause)
    }
    
    func unpauseGame()
    {
        self.removeOverlay()
        self.isPaused = false
    }
    
    private func addOverlay(ofType type: TowerSceneOverlay)
    {
        
//        let overlayURL = Bundle.main.url(forResource: type.rawValue, withExtension: "sks")
//        let overlayScene = SKReferenceNode(url: overlayURL)
        guard let overlayScene = SKNode(fileNamed: type.rawValue) else { return }
        overlayScene.name = "overlay"
        overlayScene.physicsBody = nil
        overlayScene.removeFromParent()
        overlayScene.isUserInteractionEnabled = true
        
        self.uiNode?.addChild(overlayScene)
        overlayScene.zPosition = SceneZPosition.pauseScene.rawValue
    }
    
    private func removeOverlay()
    {
        self.childNode(withName: "//overlay")?.removeFromParent()
    }
    
    func stopGame()
    {
        self.solidifyAllBlocks()
        self.entity?.removeComponent(ofType: NewBlockComponent.self)
        self.activeBlockNode = nil
    }
    
    func showGameOverScreen()
    {
        self.addOverlay(ofType: .gameOver)
    }
    
    func backToTitle()
    {
        guard let newTitleScene = GKScene(fileNamed: "TitleScene") else { return }
        (newTitleScene.rootNode as? TitleScene)?.gkScene = newTitleScene
        newTitleScene.reloadEntities()
        self.view?.presentScene((newTitleScene.rootNode as? SKScene)!, transition: SKTransition.push(with: .down, duration: 1.75))
    }
    
    private func solidifyAllBlocks()
    {
        guard let sceneEntities = self.gkScene?.entities else { return }
        for nextEntity in sceneEntities
        {
            nextEntity.component(ofType: GridComponent.self)?.solidify()
        }
    }
    
    fileprivate func timerFormatter() -> NumberFormatter
    {
        let timerFormatter = NumberFormatter()
        timerFormatter.minimumIntegerDigits     = 1
        timerFormatter.minimumFractionDigits    = 1
        timerFormatter.maximumFractionDigits    = 1
        
        return timerFormatter
    }
    
    fileprivate func checkForMatchIncrease()
    {
        if let towerRuleSystem = self.entity?.component(ofType: RuleSystemComponent.self)?.content,
            let currentTowerComponent = self.entity?.component(ofType: TowerComponent.self)
        {
            if towerRuleSystem.grade(forFact: "increaseMatchCount" as NSObjectProtocol) >= 1.0
            {
                currentTowerComponent.maxMatches += 1
                towerRuleSystem.state["nextMatchCountBenchmark"] = self.blockClearCount + currentTowerComponent.matchIncreaseCount
            }
        }
    }
    
    fileprivate func checkForRuneIncrease()
    {
        if let towerRuleSystem = self.entity?.component(ofType: RuleSystemComponent.self)?.content,
            let currentTowerComponent = self.entity?.component(ofType: TowerComponent.self)
        {
            if towerRuleSystem.grade(forFact: "increaseRuneCount" as NSObjectProtocol) >= 1.0
            {
                guard currentTowerComponent.maxRuneCount > currentTowerComponent.runeCount else { return }
                currentTowerComponent.runeCount += 1
                towerRuleSystem.state["nextRuneCountBenchmark"] = self.blockClearCount + currentTowerComponent.runeIncreaseCount
            }
        }
    }
    
    fileprivate func updateHeightDisplay()
    {
        if let currentTowerComponent = self.entity?.component(ofType: TowerComponent.self),
            let heightDisplay = self.heightDisplayNode as? SKLabelNode
        {
            let currentHeight = self.currentBlockCount
            let maxHeight = currentTowerComponent.maxHeight
            
            heightDisplay.text = "\(currentHeight)r"
            if currentHeight < maxHeight
            {
                heightDisplay.colorBlendFactor = 0.0
            }
            else
            {
                heightDisplay.color = .red
                heightDisplay.colorBlendFactor = 1.0
            }
        }
    }
    
//  MARK: SKScene methods
    override func didMove(to view: SKView)
    {        
//      Set up enviroment entity
        let difficultyURL = Bundle.main.url(forResource: "difficulty", withExtension: "plist") ?? URL(fileURLWithPath: "")
        let parameterDictionary = NSDictionary(contentsOf: difficultyURL)?["normal"] as? [String: AnyObject]
        let environmentEntity = Entities.newTower(withParameters: parameterDictionary)
        environmentEntity.addComponent(GKSKNodeComponent(node: self))
        self.gkScene?.addEntity(environmentEntity)
        self.entity?.component(ofType: NewBlockComponent.self)?.action =
        {
            _ in
            self.dropNewBlock()
        }
        self.entity?.component(ofType: StateMachineComponent.self)?.content.state(forClass: GameOverState.self)?.towerScene = self
        self.entity?.component(ofType: StateMachineComponent.self)?.content.state(forClass: PauseState.self)?.towerScene = self
        
//      Set up stack
        (self.stackNode as? StackNode)?.layoutSpaces()
        self.stackNode?.entity?.component(ofType: ReservoirComponent.self)?.fillAll()
//      Set up pause button
        self.pauseButtonNode?.isUserInteractionEnabled = true
        (self.pauseButtonNode as? ButtonNode)?.action =
        {
            self.togglePause()
        }
        self.uiNode?.isUserInteractionEnabled = true
        
//      Set up physics delegation
        self.physicsWorld.contactDelegate = self
      
        self.entity?.component(ofType: TowerComponent.self)?.blockSize = CGSize(width: 375.0,
                                                                           height: 375.0)
        
//      Setup timer
        self.nextBlockTimerNode?.isHidden = true
//      Setup height display
        let maxHeight = self.entity?.component(ofType: TowerComponent.self)?.maxHeight ?? 0
        (self.childNode(withName: "//max_height_display") as? SKLabelNode)?.text = "\(maxHeight)r"
        
//      Setup camera
        if let cameraNode = self.camera
        {
            cameraNode.constraints = [SKConstraint]()
//          Restrict to no lower than starting position
            cameraNode.constraints?.append(SKConstraint.positionY(SKRange(lowerLimit: cameraNode.position.y)))
        }
        if let currentTowerComponent = self.entity?.component(ofType: TowerComponent.self),
            let currentPedestalNode = self.pedestalNode,
            let blockSize = currentTowerComponent.blockSize
        {
            let pedestalNodeTop = currentPedestalNode.position.y + currentPedestalNode.frame.height / 2.0
//          Create starting point for blocks
            let startingNode = SKNode()
            startingNode.name = TOWER_SCENE_NODE_NAME_BLOCK_START
            startingNode.position.x = currentPedestalNode.position.x
            startingNode.position.y = pedestalNodeTop + CGFloat(currentTowerComponent.maxHeight + 2) * blockSize.height
            self.addChild(startingNode)
//          Create limit line for tower
            let limitPath = CGMutablePath()
            limitPath.move(to: CGPoint(x: -self.frame.height / 2.0, y: 0.0))
            limitPath.addLine(to: CGPoint(x: self.frame.height / 2.0, y: 0.0))
            let limitNode = SKShapeNode(path: limitPath)
            limitNode.position.x = currentPedestalNode.position.x
            limitNode.position.y = pedestalNodeTop + CGFloat(currentTowerComponent.maxHeight) * blockSize.height
            self.addChild(limitNode)
        }
        
//      Create the starting block
        self.dropNewBlock()
    }
    
    override func update(_ currentTime: TimeInterval)
    {
        let deltaTime = self.lastUpdateTime != nil ? currentTime - self.lastUpdateTime! : 0
        
//      TODO: Replace with iteration through gkScene.entities
        self.entity?.update(deltaTime: deltaTime)
        self.activeBlockNode?.entity?.update(deltaTime: deltaTime)
        
//      Check rules
//      TODO: Move
        if let currentTowerComponent = self.entity?.component(ofType: TowerComponent.self),
            let currentBlockSize = self.entity?.component(ofType: TowerComponent.self)?.blockSize
        {
            let maxHeight = CGFloat(currentTowerComponent.maxHeight) * currentBlockSize.height
            self.entity?.component(ofType: RuleSystemComponent.self)?.content.state["maxHeight"] = maxHeight
        }
        else
        {
            self.entity?.component(ofType: RuleSystemComponent.self)?.content.state["maxHeight"] = nil
        }
        
        if let blockRuleSystem = self.activeBlockNode?.entity?.component(ofType: RuleSystemComponent.self)?.content
        {
            blockRuleSystem.reset()
            blockRuleSystem.evaluate()
        }
        self.activeBlockNode?.entity?.component(ofType: GridComponent.self)?.countMatches()
        self.activeBlockNode?.entity?.component(ofType: GridComponent.self)?.checkTarget()
        self.activeBlockNode?.entity?.component(ofType: GridComponent.self)?.checkFilled()
        
        if let towerRuleSystem = self.entity?.component(ofType: RuleSystemComponent.self)?.content
        {
            towerRuleSystem.reset()
            towerRuleSystem.evaluate()
        }
        
//      Update views
        if let towerNewBlockComponent = self.entity?.component(ofType: NewBlockComponent.self)
        {
//          Update timer
            let timeLeft = towerNewBlockComponent.timeLeft
            self.nextBlockTimerNode?.isHidden = timeLeft > 3.0
            (self.nextBlockTimerNode as? SKLabelNode)?.text = self.timerFormatter().string(from: NSNumber(value: timeLeft))
        }
        
        self.checkForMatchIncrease()
        self.checkForRuneIncrease()
        let tooHigh = self.entity?.component(ofType: RuleSystemComponent.self)?.content.facts.contains
        {
            return $0 as? String == "tooHigh"
        }
        if tooHigh ?? false
        {
            self.entity?.component(ofType: StateMachineComponent.self)?.content.enter(GameOverState.self)
        }
        
        self.lastUpdateTime = currentTime
    }
    
    
}

//MARK: SKPhysicsContactDelegate conformance
extension TowerScene : SKPhysicsContactDelegate
{
    func didBegin(_ contact: SKPhysicsContact)
    {
        let bodyACategory = TowerCategoryOptionSet(rawValue: contact.bodyA.categoryBitMask)
        let bodyBCategory = TowerCategoryOptionSet(rawValue: contact.bodyB.categoryBitMask)
        
        if bodyACategory.contains(.tower)
        {
            if let fallingBlockNode = contact.bodyB.node as? BlockNode
            {
                self.activeBlockNode = fallingBlockNode
            }
        }
        if bodyACategory.contains(.block) && bodyBCategory.contains(.block)
        {
            if let blockA = contact.bodyA.node as? BlockNode,
                let blockB = contact.bodyB.node as? BlockNode
            {
                self.entity?.component(ofType: RuleSystemComponent.self)?.content.state["currentTowerHeight"] = max(blockA.position.y, blockB.position.y)
            }
        }
        self.updateHeightDisplay()
    }
}
