//
//  ElementComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/16/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class ElementComponent: GKComponent
{
/// The key used in `NSCoder` objects for NSCoding comformance
    private let ELEMENT_COMPONENT_CODING_KEY_ELEMENT_BITMASK = "ElementComponent.elementBitmask"
    
    struct Element : OptionSet
    {
        let rawValue: UInt32
    }
    
    var element: Element = Element(rawValue: 0)
    @GKInspectable var elementBitmask : UInt32
    {
        get
        {
            return element.rawValue
        }
        set
        {
            self.element = Element(rawValue: newValue)
        }
    }
    
    init(withElement element: Element)
    {
        self.element = element
        
        super.init()
    }
    
    override init()
    {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
//      TODO: Should be able to use the same key as GKInspectable uses to set values after loading from file. Find out how
        if aDecoder.containsValue(forKey: ELEMENT_COMPONENT_CODING_KEY_ELEMENT_BITMASK)
        {
            let bitmask = aDecoder.decodeCInt(forKey: ELEMENT_COMPONENT_CODING_KEY_ELEMENT_BITMASK)
            self.element = Element(rawValue: UInt32(bitmask))
        }
    }
    
//  MARK: NSCoding conformance
    override func encode(with aCoder: NSCoder)
    {
        super.encode(with: aCoder)
        aCoder.encode(Int(self.element.rawValue), forKey: ELEMENT_COMPONENT_CODING_KEY_ELEMENT_BITMASK)
    }
}
