//
//  IdlePieceState.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/28/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class IdlePieceState: GKState
{
    override func isValidNextState(_ stateClass: AnyClass) -> Bool
    {
        switch stateClass
        {
        case is DroppingPieceState.Type:
            return true
//        case is MatchingState.Type:
        default:
            return false
        }
    }
    
    override func didEnter(from previousState: GKState?)
    {
        
    }
}
