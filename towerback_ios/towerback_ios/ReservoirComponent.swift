//
//  ReservoirComponent
//  towerback_ios
//
//  Created by Duncan Oliver on 8/16/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class ReservoirComponent : GKComponent
{
//  TODO: Make private?
    var spaces          = [GKEntity]()
    let waitingSpace    = Entities.newSpace()
    @GKInspectable var count : Int
    {
        get
        {
            return spaces.count
        }
        set(newCount)
        {
            while spaces.count > newCount
            {
                self.spaces.removeLast()
            }
            while spaces.count < newCount
            {
                self.spaces.append(Entities.newSpace())
            }
            fillAll()
        }
    }
    
    override init()
    {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
        
//  MARK: -
    func nextPiece() -> GKEntity?
    {
        let nextPiece = self.spaces.first?.component(ofType: HoldComponent.self)?.piece
        nextPiece?.component(ofType: PieceComponent.self)?.moveTo(self.waitingSpace)
        advance()
        return nextPiece
    }
    
    func fillAll()
    {
        for nextSpace in self.spaces
        {
            self.fill(nextSpace)
        }
    }
    
    fileprivate func fill(_ space: GKEntity)
    {
        guard space.component(ofType: HoldComponent.self)?.piece == nil else { return }
//      Get current rune count
        let newPiece : GKEntity?
//      TODO: Move
        if let currentTower = (self.entity?.component(ofType: GKSKNodeComponent.self)?.node.scene as? TowerScene)?.entity?.component(ofType: TowerComponent.self)
        {
            newPiece = PieceMaker.nextPiece(withUpperBound: currentTower.runeCount)
        }
        else
        {
            newPiece = PieceMaker.nextPiece()
        }
        newPiece?.component(ofType: PieceComponent.self)?.moveTo(space)
    }
    
    fileprivate func advance()
    {
        let firstSpace = self.spaces.removeFirst()
        
        self.fill(firstSpace)
        
        self.spaces.append(firstSpace)
        
        if let stackNode = self.entity?.component(ofType: GKSKNodeComponent.self)?.node as? StackNode
        {
            stackNode.layoutSpaces()
        }
    }
    
    override func didAddToEntity()
    {
//        super.didAddToEntity()
//        if let stackNode = self.entity?.component(ofType: GKSKNodeComponent.self)?.node as? StackNode
//        {
//            stackNode.layoutSpaces()
//        }
    }
}
