//
//  PauseState.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/5/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class PauseState: GKState
{
    var towerScene : TowerScene?
    
    override func isValidNextState(_ stateClass: AnyClass) -> Bool
    {
        switch stateClass
        {
        case is PlayingState.Type:
            return true
        case is PauseState.Type:
            return false
        case is GameOverState.Type:
            return false
        default:
            return false
        }
    }
    
    override func didEnter(from previousState: GKState?)
    {
        self.towerScene?.pauseGame()
    }
    
    override func willExit(to nextState: GKState)
    {
        self.towerScene?.unpauseGame()
    }
}
