//
//  GridComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/23/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit


class GridComponent: GKComponent
{
    var grid : Grid<GKEntity>
    
    init(numberOfColumns columns: Int, numberOfRows rows: Int)
    {
        self.grid = Grid<GKEntity>(withRanges: 0..<columns, 0..<rows)
        
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func countMatches()
    {
        guard !self.isAnimating() else { return }
        if let gridRuleSystem = self.entity?.component(ofType: RuleSystemComponent.self)?.content
        {
//          Filter here
            var matchCount = 0
            let matchedFacts = gridRuleSystem.facts.filter(isMatchedFact)
            for nextFact in matchedFacts
            {
                guard let factName = nextFact as? String
                    , gridRuleSystem.grade(forFact: factName as NSObjectProtocol) == 1.0
                    else { continue }
                
                let factComponents = factName.components(separatedBy: ".")
                if factComponents[0] == "matched"
                {
                    guard let column = Int(factComponents[1]),
                        let row = Int(factComponents[2]) else { continue }
                    self.grid[column, row]?.component(ofType: HoldComponent.self)?.destroy()
                    
                    gridRuleSystem.retractFact(factName as NSObjectProtocol)
                    matchCount += 1
                }
            }
//          Count matches
            if let gemComponent = self.entity?.component(ofType: GemComponent.self)
            {
                var gemValue = 0
                switch matchCount
                {
                case 0:
                    break
                case let count where count < 4:
                    gemValue = 1
                case 4:
                    gemValue = 2
                default:
                    gemValue = 3
                }
                if gemValue > 0
                {
                    gemComponent.add(gemValue)
                    (self.entity?.component(ofType: GKSKNodeComponent.self)?.node.childNode(withName: "meter") as? StockNode)?.addGem(gemValue)
                }
            }
            
        }
    }
    
    func checkTarget()
    {
        guard !self.isAnimating() else { return }
        if self.entity?.component(ofType: GemComponent.self)?.full ?? false
        {
            self.destroy()
        }
    }
    
    func checkFilled()
    {
        guard !self.isAnimating() else { return }
        self.updateElementStates()
        guard let blockRuleSystem = self.entity?.component(ofType: RuleSystemComponent.self)?.content else { return }
        if blockRuleSystem.grade(forFact: "filled" as NSObjectProtocol) == 1.0 && blockRuleSystem.facts.filter(isMatchedFact).count == 0
        {
            self.solidify()
        }
    }
    
    func destroy()
    {
        (self.entity?.component(ofType: GKSKNodeComponent.self)?.node.scene as? TowerScene)?.increaseBlockCount()
        self.entity?.component(ofType: GKSKNodeComponent.self)?.node.removeFromParent()
    }
    
    func solidify()
    {
//      Change appearance
        if let currentNodeComponent = self.entity?.component(ofType: GKSKNodeComponent.self),
            let currentBlockNode = currentNodeComponent.node as? BlockNode
        {
            currentBlockNode.texture = SKTexture(image: #imageLiteral(resourceName: "block-solid"))
            currentBlockNode.enumerateChildNodes(withName: ".//piece*")
            {
                node, stop in
                (node as? SKSpriteNode)?.texture = SKTexture(image: #imageLiteral(resourceName: "piece-solid"))
            }
        }
        
//      Find next active block
        if let solidBlockNode = self.entity?.component(ofType: GKSKNodeComponent.self)?.node,
            let blockPhysicsBody = solidBlockNode.physicsBody
        {
            var newOptionSet = TowerCategoryOptionSet(rawValue: blockPhysicsBody.categoryBitMask)
            newOptionSet.insert(.tower)
            blockPhysicsBody.categoryBitMask = newOptionSet.bitmask
//          Check for next active block
            let connectedBodies = blockPhysicsBody.allContactedBodies()
            for nextBody in connectedBodies
            {
                guard let nextBlockNode = nextBody.node as? BlockNode else { return }
                let connectBodyCategory = TowerCategoryOptionSet(rawValue: nextBody.categoryBitMask)
                if connectBodyCategory.contains(.block) && !connectBodyCategory.contains(.tower)
                {
                    if let currentTowerScene = solidBlockNode.scene as? TowerScene
                    {
                        currentTowerScene.activeBlockNode = nextBlockNode
                    }
                    break
                }
            }
        }
    }
    
    fileprivate func updateElementStates()
    {
        var updatedElementDictionary = [String : Int]()
        if let columnRange = self.grid.ranges?[0],
            let rowRange = self.grid.ranges?[1]
        {
            for nextColumn in columnRange
            {
                for nextRow in rowRange
                {
                    guard let nextElement = self.grid[nextColumn, nextRow]?.component(ofType: HoldComponent.self)?.piece?.component(ofType: ElementComponent.self)?.element
                        else { continue }
                    updatedElementDictionary["\(nextColumn)_\(nextRow)"] = Int(nextElement.rawValue)
                }
            }
        }
        self.entity?.component(ofType: RuleSystemComponent.self)?.content.state["element"] = updatedElementDictionary
    }
    
    fileprivate func updateMeter()
    {
        if let count = self.entity?.component(ofType: GemComponent.self)?.values.count
        {
            (self.entity?.component(ofType: GKSKNodeComponent.self)?.node.childNode(withName: "meter") as? StockNode)?.chargeTotal = count
        }
    }
    
    fileprivate func isMatchedFact(_ fact: Any) -> Bool
    {
        if let factString = fact as? String,
            factString.hasPrefix("matched")
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    
    private func isAnimating() -> Bool
    {
        if let columnRange = self.grid.ranges?[0],
            let rowRange = self.grid.ranges?[1]
        {
            for nextColumn in columnRange
            {
                for nextRow in rowRange
                {
                    guard let nextPiece = self.grid[nextColumn, nextRow]?.component(ofType: HoldComponent.self)?.piece else { continue }
                    if let _ = nextPiece.component(ofType: AnimationStateMachineComponent.self)?.content.currentState as? IdlePieceState
                    {
                        continue
                    }
                    else
                    {
                        return true
                    }
                }
            }
        }
        
        return false
    }
    
    override func update(deltaTime seconds: TimeInterval)
    {
        updateElementStates()
        updateMeter()
    }
}
