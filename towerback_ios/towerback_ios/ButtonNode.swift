//
//  ButtonNode.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/21/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import SpriteKit


/**
 `ButtonNode` runs a specified closure when it detects a touch. Best used as an encompassing node, containing other nodes defining the appearance.
 */
class ButtonNode: SKNode
{
///
    var action : (() -> Void)?
    enum InteractionActionKey : String
    {
        case began      = "began"
        case moved      = "moved"
        case ended      = "ended"
        case cancelled  = "cancelled"
    }
    var interactionActions = [InteractionActionKey : SKAction]()
    
// TODO: Set `userInteractionEnabled` on initialization?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if let beganAction = self.interactionActions[.began]
        {
            self.run(beganAction, withKey: "ButtonNode.action.began")
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if let endedAction = self.interactionActions[.ended]
        {
            self.run(endedAction, withKey: "ButtonNode.action.ended")
        }
        self.action?()
    }
}
