//
//  TickerComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/29/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class TickerComponent: GKComponent
{
    var target : Int?
//  Hide
    var value : Int = 0
    
    func tick(by increase: Int = 1)
    {
        value += increase
    }
    
    func hasHitTarget() -> Bool
    {
        return value >= target
    }
}
