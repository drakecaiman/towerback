//
//  GameSceneView.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 2/8/17.
//  Copyright © 2017 Highjacks. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameSceneView: SKView
{
    func loadTower(withTransition transition: SKTransition = SKTransition.push(with: .up, duration: 1.75))
    {
//      TODO: drop button on press
//      self.playNode?.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 1.0, height: 1.0))
        
        guard let newTowerScene = GKScene(fileNamed: "TowerScene") else { return }
        (newTowerScene.rootNode as? TowerScene)?.gkScene = newTowerScene
        newTowerScene.reloadEntities()
        self.presentScene((newTowerScene.rootNode as? SKScene)!, transition: transition)
    }
}
