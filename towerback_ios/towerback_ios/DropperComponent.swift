//
//  Dropper.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/27/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class DropperComponent : GKComponent
{
    func dropPiece(_ piece: GKEntity, into destination: GKEntity)
    {
        guard let destinationDropComponent = destination.component(ofType: HoldComponent.self),
            let dropPieceComponent = piece.component(ofType: PieceComponent.self)
            , destinationDropComponent.piece == nil
            else { return }
        guard let dropPieceNode = piece.component(ofType: GKSKNodeComponent.self)?.node as? PieceNode,
            let destinationNode = destination.component(ofType: GKSKNodeComponent.self)?.node
            else { return }
        
        dropPieceComponent.moveTo(destination)
        
        dropPieceNode.removeFromParent()
        destinationNode.addChild(dropPieceNode)
        
        dropPieceNode.materialize()
        piece.component(ofType: AnimationStateMachineComponent.self)?.content.enter(DroppingPieceState.self)
    }
}
