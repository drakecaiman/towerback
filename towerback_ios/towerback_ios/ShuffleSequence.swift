//
//  ShuffleSequence.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/28/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import Foundation

enum ShuffleSequenceError : Error
{
    case empty
}

class ShuffleSequence<T>
{
    let items : [T]
    var queue = [T]()
    
    init(withItems items: [T]) throws
    {
        if !(items.count > 0)
        {
            throw ShuffleSequenceError.empty
        }
        
        self.items = items
        
        self.addShuffle()
    }
    
    private func addShuffle()
    {
        var newItems = [T](items)
        
        while newItems.count > 0
        {
            let randomIndex = Int(arc4random_uniform(UInt32(newItems.count)))
            let nextItem = newItems.remove(at: randomIndex)
            self.queue.append(nextItem)
        }
    }
    
    func next() -> T
    {
        if !(self.queue.count > 0)
        {
            self.addShuffle()
        }
        
        return self.queue.removeFirst()
    }
}
