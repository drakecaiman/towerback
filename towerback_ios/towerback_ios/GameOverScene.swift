//
//  GameOverScene.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 2/7/17.
//  Copyright © 2017 Highjacks. All rights reserved.
//

import SpriteKit

class GameOverScene : SKScene
{
/// The node name for the resume button
    let TOWER_SCENE_NODE_NAME_RESTART_BUTTON = "button.restart"
/// The node name for the exit button
    let TOWER_SCENE_NODE_NAME_TITLE_BUTTON   = "button.title"
    
/// The node used as a resume button
    var restartButtonNode : SKNode?
    {
        return self.childNode(withName: "//\(TOWER_SCENE_NODE_NAME_RESTART_BUTTON)")
    }
/// The node used as a exit button
    var titleButtonNode : SKNode?
    {
        return self.childNode(withName: "//\(TOWER_SCENE_NODE_NAME_TITLE_BUTTON)")
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
//      Set up resume button
        self.restartButtonNode?.isUserInteractionEnabled = true
        (self.restartButtonNode as? ButtonNode)?.action =
        {
            let transition = SKTransition.crossFade(withDuration: 0.75)
            (self.childNode(withName: "/.")?.scene?.view as? GameSceneView)?.loadTower(withTransition: transition)
//            (self.childNode(withName: "/.") as? TowerScene)?.backToTitle()
        }
//      Set up resume button
        self.titleButtonNode?.isUserInteractionEnabled = true
        (self.titleButtonNode as? ButtonNode)?.action =
        {
            (self.childNode(withName: "/.") as? TowerScene)?.backToTitle()
        }
    }
}
