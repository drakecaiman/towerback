//
//  PieceNode.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/17/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import SpriteKit

class PieceNode: SKSpriteNode
{
    func materialize()
    {
        self.size = self.parent?.frame.size ?? CGSize.zero
        self.setScale(1.23)
        self.alpha = 0.0
    }
}
