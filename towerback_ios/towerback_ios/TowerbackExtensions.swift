//
//  TowerbackExtensions.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/18/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

extension GKScene
{
    func reloadEntities()
    {
        for nextEntity in self.entities
        {
            self.removeEntity(nextEntity)
            if let nextNode = nextEntity.component(ofType: GKSKNodeComponent.self)?.node
            {
                nextEntity.addComponent(GKSKNodeComponent(node: nextNode))
            }
            self.addEntity(nextEntity)
        }
    }
}
