//
//  GameOverState.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/5/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class GameOverState: GKState
{
    var towerScene : TowerScene?
    
    override func isValidNextState(_ stateClass: AnyClass) -> Bool
    {
        switch stateClass
        {
//        case is PlayingState.Type:
//            fallthrough
//        case is PauseState.Type:
//            fallthrough
//        case is GameOverState.Type:
//            fallthrough
        default:
            return false
        }
    }
    
    override func didEnter(from previousState: GKState?)
    {
        self.towerScene?.stopGame()
        self.towerScene?.showGameOverScreen()
    }
}
