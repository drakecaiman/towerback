//
//  OwnerComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/28/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

/**
 The `OwnerComponent` is a simple generic component for attaching objects to `GKEntity` objects. Use this to easily give an entity access to other important GameplayKit objects like `GKRuleSystem` and `GKStateMachine`.
 */
class OwnerComponent<T> : GKComponent
{
//  MARK: Properties
/// The objected owned by this entity
    let content : T
    
//  MARK: Initializers
    
    
//  TODO: Figure out
    /**
     
     - returns:
     */
//    override init()
//    {
//        self.content = T()
//        
//        super.init()
//    }
    
    /**
     Create a new component with the specified object attached.
     
     - parameter content: The object to attach
     */
    init(with content: T)
    {
        self.content = content
        
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
