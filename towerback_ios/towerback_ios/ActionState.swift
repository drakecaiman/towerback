//
//  ActionState.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/28/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit
import SpriteKit

class ActionState: GKState
{
    var node : SKNode?
    var action : SKAction?
    
    func triggerAction(_ completionBlock: (() -> Void)? = nil)
    {
        guard let stateAction = self.action else
        {
            print("No action found")
            return
        }
        if let completion = completionBlock
        {
            self.node?.run(stateAction, completion: completion)
        }
        else
        {
            self.node?.run(stateAction)
        }
    }
}
