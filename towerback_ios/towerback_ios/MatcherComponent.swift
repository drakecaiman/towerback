//
//  Matcher.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/23/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class MatcherComponent : GKComponent
{
    var chainLength = 3
    
//  Condense
    func predicateForMatchingSpaceAtColumn(_ column: Int, row: Int) -> NSPredicate
    {
        let elementColumnStartRange = (column - (chainLength - 1))...column
        var columnPredicates = [NSPredicate]()
        for nextColumnStart in elementColumnStartRange
        {
            var columnPairSubpredicates = [NSPredicate]()
            for nextColumnIndex in nextColumnStart..<(nextColumnStart + (chainLength - 1))
            {
                let leftPath    = "state.element.\(nextColumnIndex)_\(row)"
                let rightPath   = "state.element.\(nextColumnIndex + 1)_\(row)"
                columnPairSubpredicates.append(NSPredicate(format: "%K != NIL && %K == %K", leftPath, leftPath, rightPath))
            }
            let chainPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: columnPairSubpredicates)
            columnPredicates.append(chainPredicate)
        }
        let columnMatchPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: columnPredicates)
        
        let elementRowStartRange = (row - (chainLength - 1))...row
        var rowPredicates = [NSPredicate]()
        for nextRowStart in elementRowStartRange
        {
            var rowPairSubpredicates = [NSPredicate]()
            for nextRowIndex in nextRowStart..<(nextRowStart + (chainLength - 1))
            {
                let leftPath    = "state.element.\(column)_\(nextRowIndex)"
                let rightPath   = "state.element.\(column)_\(nextRowIndex + 1)"
                rowPairSubpredicates.append(NSPredicate(format: "%K != NIL && %K == %K", leftPath, leftPath, rightPath))
            }
            let chainPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: rowPairSubpredicates)
            rowPredicates.append(chainPredicate)
        }
        let rowMatchPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: rowPredicates)
        
        return NSCompoundPredicate(orPredicateWithSubpredicates: [columnMatchPredicate, rowMatchPredicate])
    }
}
