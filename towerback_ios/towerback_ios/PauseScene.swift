//
//  PauseScene.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 2/7/17.
//  Copyright © 2017 Highjacks. All rights reserved.
//

import SpriteKit

class PauseScene: SKScene
{
/// The node name for the resume button
    let TOWER_SCENE_NODE_NAME_RESUME_BUTTON = "button.resume"
/// The node name for the exit button
    let TOWER_SCENE_NODE_NAME_EXIT_BUTTON   = "button.exit"
    
/// The node used as a resume button
    var resumeButtonNode : SKNode?
    {
        return self.childNode(withName: "//\(TOWER_SCENE_NODE_NAME_RESUME_BUTTON)")
    }
/// The node used as a exit button
    var exitButtonNode : SKNode?
    {
        return self.childNode(withName: "//\(TOWER_SCENE_NODE_NAME_EXIT_BUTTON)")
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
//      Set up resume button
        self.resumeButtonNode?.isUserInteractionEnabled = true
        (self.resumeButtonNode as? ButtonNode)?.action =
        {
            (self.childNode(withName: "/.") as? TowerScene)?.unpauseGame()
        }
//      Set up resume button
        self.exitButtonNode?.isUserInteractionEnabled = true
        (self.exitButtonNode as? ButtonNode)?.action =
        {
            (self.childNode(withName: "/.") as? TowerScene)?.backToTitle()
        }
    }
}
