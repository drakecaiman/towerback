//
//  StackNode.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/16/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import SpriteKit
import GameplayKit

class StackNode: SKNode
{
    fileprivate let STACK_NODE_SPACE_NODE_NAME = "space"
    
    var spaceSize = CGSize(width: 24.0, height: 24.0)
    var spacePadding : CGFloat = 8.0
    enum StackOrientation
    {
        case up
        case down
        case left
        case right
    }
    var orientation : StackOrientation = .right
    
    func layoutSpaces()
    {
        guard let stack = self.entity?.component(ofType: ReservoirComponent.self)?.spaces else { return }
        
        var nextXPosition = self.spaceSize.width / 2.0
        var nextYPosition = self.spaceSize.height / 2.0

        for (nextIndex, nextSpace) in stack.enumerated()
        {
//          Set up node for next space
            if let existingSpaceNode = nextSpace.component(ofType: GKSKNodeComponent.self)?.node as? SpaceNode
            {
                existingSpaceNode.size = self.spaceSize
                existingSpaceNode.name = "\(STACK_NODE_SPACE_NODE_NAME).\(nextIndex)"
                existingSpaceNode.removeFromParent()
                self.addChild(existingSpaceNode)
                
//              Check for piece node
                if let nextSpacePiece = nextSpace.component(ofType: HoldComponent.self)?.piece,
                    let nextSpacePieceNode = nextSpacePiece.component(ofType: GKSKNodeComponent.self)?.node as? PieceNode
                {
                    nextSpacePieceNode.size = self.spaceSize
                    nextSpacePieceNode.removeFromParent()
                    existingSpaceNode.addChild(nextSpacePieceNode)
                }
                
    //          Move space to new position
                existingSpaceNode.run(SKAction.move(to: CGPoint(x: nextXPosition, y: nextYPosition), duration: 0.34))
            }

            var xOffset : CGFloat = 0.0
            var yOffset : CGFloat = 0.0
            switch self.orientation
            {
            case .up:
                yOffset = (self.spacePadding + self.spaceSize.height)
            case .down:
                yOffset = -(self.spacePadding + self.spaceSize.height)
            case .right:
                xOffset = (self.spacePadding + self.spaceSize.width)
            case .left:
                xOffset = -self.spacePadding + self.spaceSize.width
            }
            nextXPosition += xOffset
            nextYPosition += yOffset
        }
    }
}
