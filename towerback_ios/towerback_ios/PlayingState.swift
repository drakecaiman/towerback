//
//  PlayingState.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/5/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class PlayingState: GKState
{
    override func isValidNextState(_ stateClass: AnyClass) -> Bool
    {
        switch stateClass
        {
        case is PlayingState.Type:
            return false
        case is PauseState.Type:
            return true
        case is GameOverState.Type:
            return true
        default:
            return false
        }
    }
    
    override func didEnter(from previousState: GKState?)
    {
        
    }
}
