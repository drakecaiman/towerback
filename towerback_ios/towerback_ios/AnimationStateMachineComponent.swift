//
//  AnimationStateMachineComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/28/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class AnimationStateMachineComponent: StateMachineComponent
{
//  TODO: Update to use
    var node : SKNode?
    {
        return self.entity?.component(ofType: GKSKNodeComponent.self)?.node
    }
}


/*
extension AnimationStateMachineComponent : ActionStateDelegate
{
    func actionStateDidEnter(_ actionState: ActionState)
    {
        guard let currentNode = self.node else
        {
            print("No node found")
            return
        }
        guard let currentAction = actionState.action else
        {
            print("No action found on state")
            return
        }
        
        currentNode.run(currentAction)
    }
}

protocol ActionStateDelegate
{
    func actionStateDidEnter(_ actionState: ActionState)
}
 */
