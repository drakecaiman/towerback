//
//  HoldComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/16/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class HoldComponent: GKComponent
{
    var piece : GKEntity?
    
    func destroy()
    {
        self.piece?.component(ofType: GKSKNodeComponent.self)?.node.removeFromParent()
        self.piece = nil
    }
}
