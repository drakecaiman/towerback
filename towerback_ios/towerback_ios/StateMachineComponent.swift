//
//  GKSKNodeComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/16/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

/**
 `StateMachineComponent` objects allow you to attach `GKStateMachine` objects to `GKEntity` objects.
 */
class StateMachineComponent : OwnerComponent<GKStateMachine>
{
//  TODO: Bug currently prevents inheritance of generic superclass initializer. Remove when fixed.
    override init(with content: GKStateMachine)
    {
        super.init(with: content)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
}
