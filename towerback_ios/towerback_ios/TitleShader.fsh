vec3 hToRGB(float h)
{
    float r = 0.0;
    float g = 0.0;
    float b = 0.0;
    
    if (h >= (0.0 / 6.0) && h <= (1.0 / 6.0))
    {
        r = 1.0;
    }
    else if (h >= (5.0 / 6.0) && h <= (6.0 / 6.0))
    {
        r = 1.0;
    }
    else if (h >= (1.0 / 6.0) && h <= (2.0 / 6.0))
    {
        r = mix(1.0, 0.0, h * 6.0 - 1.0);
    }
    else if (h >= (4.0 / 6.0) && h <= (5.0 / 6.0))
    {
        r = mix(0.0, 1.0, h * 6.0 - 4.0);
    }
    
    if (h >= (1.0 / 6.0) && h <= (3.0 / 6.0))
    {
        g = 1.0;
    }
    else if (h >= (3.0 / 6.0) && h <= (4.0 / 6.0))
    {
        g = mix(1.0, 0.0, h * 6.0 - 3.0);
    }
    else if (h >= (0.0 / 6.0) && h <= (1.0 / 6.0))
    {
        g = mix(0.0, 1.0, h * 6.0);
    }
    
    if (h >= (3.0 / 6.0) && h <= (5.0 / 6.0))
    {
        b = 1.0;
    }
    else if (h >= (5.0 / 6.0) && h <= (6.0 / 6.0))
    {
        b = mix(1.0, 0.0, h * 6.0 - 5.0);
    }
    else if (h >= (2.0 / 6.0) && h <= (3.0 / 6.0))
    {
        b = mix(0.0, 1.0, h * 6.0 - 2.0);
    }
    
    return vec3(r,g,b);
}

void main()
{
    vec4 pre_color = SKDefaultShading();
    
    float offset_time   = u_time + v_tex_coord.y;
    float cycle         = fract(offset_time / color_rotation_cycle_time);
    
    vec3 rgb = hToRGB(cycle);
    vec3 adjusted_rgb = rgb * pre_color.rgb;

//  TODO: Replace only white
    if (pre_color.r == 0.0 && pre_color.g == 0.0 && pre_color.b == 0.0)
    {
        gl_FragColor = vec4(adjusted_rgb.r, adjusted_rgb.g, adjusted_rgb.b, pre_color.a);
    }
    else
    {
        gl_FragColor = pre_color
    }
}

