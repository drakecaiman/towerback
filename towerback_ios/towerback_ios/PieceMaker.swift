//
//  PieceMaker.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/10/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import Foundation
import GameplayKit
import SpriteKit

// TODO: Always starts with Red. Fix. (upperBound not set before creating new piece in ReservoirComponent
class PieceMaker
{
    private static let PIECEMAKER_PIECE_SCENE_FILE_NAME = "Pieces"
    private static let PIECE_NODE_NAME_PREFIX = "piece"
    
    static func nextPiece(withUpperBound upperBound: Int = 0) -> GKEntity
    {
        let elementIndex = GKARC4RandomSource.sharedRandom().nextInt(upperBound: upperBound)
        return self.newPiece(elementIndex)
    }
    
//  TODO: return nil on no sprite
    private static func newPiece(_ index: Int) -> GKEntity
    {
        let newEntity : GKEntity
//      Open scene file
        if let pieceScene = GKScene(fileNamed: PIECEMAKER_PIECE_SCENE_FILE_NAME),
            let pieceRootNode = pieceScene.rootNode as? SKNode,
            let existingPieceNode = pieceRootNode.childNode(withName: "\(PIECE_NODE_NAME_PREFIX).\(index)")
        {
            pieceScene.reloadEntities()
            newEntity = existingPieceNode.entity?.copy() as? GKEntity ?? GKEntity()
        }
        else
        {
            newEntity = GKEntity()
        }
        
        if let newNode = newEntity.component(ofType: GKSKNodeComponent.self)?.node
        {
//          Setup node
            newNode.position = CGPoint.zero

//          Set up animation state machine
            let newPieceAnimationStateMachine = self.pieceAnimationStateMachine(forNode: newNode)
            newEntity.addComponent(AnimationStateMachineComponent(with: newPieceAnimationStateMachine))
        }
        
        return newEntity
    }
    
//  MARK: State machine creation
    private static func pieceAnimationStateMachine(forNode node: SKNode? = nil) -> GKStateMachine
    {
        let idleState = IdlePieceState()
        let droppingState = DroppingPieceState()
        droppingState.node = node
        droppingState.action = self.appearanceAction()
        let stateMachine = GKStateMachine(states: [idleState, droppingState])
        stateMachine.enter(IdlePieceState.self)
        
        return stateMachine
    }
    
//  MARK: Action creation
    private static func appearanceAction() -> SKAction
    {
        let fallAction = SKAction(named: "piece.appear") ?? SKAction()
//        guard let waveEmitter = self.newLandingEmitter() else
//        {
//            return SKAction()
//        }
//        let waveAction = self.particleAction(forEmitter: waveEmitter)

        return fallAction
//        return SKAction.group([fallAction, waveAction])
    }
    
    static func newLandingEmitter() -> SKEmitterNode?
    {
        guard let emitterPath = Bundle.main.path(forResource: "PieceDust", ofType: "sks"),
            let emitter = NSKeyedUnarchiver.unarchiveObject(withFile: emitterPath) as? SKEmitterNode else
        {
            return nil
        }
        
        emitter.name = "emitter.wave"
        return emitter
    }
    
    static func particleAction(forEmitter emitterNode: SKEmitterNode, withDuration desiredDuration: TimeInterval? = nil) -> SKAction
    {
//      Calculate duration
        let duration : TimeInterval
        if desiredDuration != nil
        {
            duration = desiredDuration!
        }
        else if emitterNode.numParticlesToEmit != 0
        {
            duration = TimeInterval(CGFloat(emitterNode.numParticlesToEmit) / emitterNode.particleBirthRate + (emitterNode.particleLifetime + emitterNode.particleLifetimeRange))
        }
        else
        {
            duration = 0
//          TODO: Error
            print("No duration nor finite particle system given")
        }
        
        let addAction = SKAction.customAction(withDuration: 0.033)
        {
            node, time in
            if let parent = node.parent,
                emitterNode.parent == nil
            {
//                emitterNode.targetNode = parent
                parent.addChild(emitterNode)
            }
        }
        let stallAction = SKAction.wait(forDuration: duration)
        let removeAction = SKAction.run(SKAction.removeFromParent(), onChildWithName: "emitter.wave")
        
        return SKAction.sequence([addAction, stallAction, removeAction])
    }
}
