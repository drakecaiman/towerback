//
//  SpaceNode.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/17/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import SpriteKit

class SpaceNode: SKNode
{
    var size : CGSize?
    
    override var frame: CGRect
    {
        let startingFrame = CGRect(origin: self.position, size: self.size ?? CGSize.zero)
        return startingFrame.offsetBy(dx: -startingFrame.size.width / 2.0, dy: -startingFrame.size.height / 2.0)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        guard let currentTowerScene = self.scene as? TowerScene else { return }
        currentTowerScene.dropNextPieceInto(self)
    }
}
