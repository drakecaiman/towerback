//
//  Entities.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 8/23/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit
import SpriteKit

class Entities
{
    /**
     Create a new `GKEntity` setup for use as a scene enviroment
     
     - returns: A new `GKEntity` with components and settings suitable for representing a scene enviroment
     */
    static func newTower(withParameters parameters: [String: AnyObject]? = nil) -> GKEntity
    {
        let newTowerEntity = GKEntity()
        let newTowerComponent = TowerComponent(withParameters: parameters)
        newTowerEntity.addComponent(newTowerComponent)
        newTowerEntity.addComponent(MatcherComponent())
        newTowerEntity.addComponent(DropperComponent())
        let newTimerComponent = NewBlockComponent(forInterval: newTowerComponent.newBlockInterval,
                                                  repeating: true,
                                                  started: true)
        {
            _ in
        }
        newTowerEntity.addComponent(newTimerComponent)
        let sceneStateMachine = GKStateMachine(states: [PlayingState(), PauseState(), GameOverState()])
        sceneStateMachine.enter(PlayingState.self)
        newTowerEntity.addComponent(StateMachineComponent(with: sceneStateMachine))
        let newTowerRuleSystem = self.newTowerRuleSystem()
        newTowerRuleSystem.state["nextMatchCountBenchmark"] = newTowerComponent.matchIncreaseCount
        newTowerRuleSystem.state["nextRuneCountBenchmark"] = newTowerComponent.runeIncreaseCount
        newTowerRuleSystem.state["currentBlockClearCount"] = 0
        newTowerEntity.addComponent(RuleSystemComponent(with: newTowerRuleSystem))
        
        return newTowerEntity
    }
    
    fileprivate static func newTowerRuleSystem() -> GKRuleSystem
    {
        let newRuleSystem = GKRuleSystem()
        
//      Create match increase rule
        let matchIncreasePredicate = NSPredicate(format: "%K >= %K", "state.currentBlockClearCount", "state.nextMatchCountBenchmark")
        let matchIncreaseRule = GKRule(predicate: matchIncreasePredicate, assertingFact: "increaseMatchCount" as NSObjectProtocol, grade: 1.0)
        newRuleSystem.add(matchIncreaseRule)
//      Create rune increase rule
        let runeIncreasePredicate = NSPredicate(format: "%K >= %K", "state.currentBlockClearCount", "state.nextRuneCountBenchmark")
        let runeIncreaseRule = GKRule(predicate: runeIncreasePredicate, assertingFact: "increaseRuneCount" as NSObjectProtocol, grade: 1.0)
        newRuleSystem.add(runeIncreaseRule)
//      Create height rule
        let heightPredicate = NSPredicate(format: "%K > %K", "state.currentTowerHeight", "state.maxHeight")
        let heightRule = GKRule(predicate: heightPredicate, assertingFact: "tooHigh" as NSObjectProtocol, grade: 1.0)
        heightRule.salience = 0
        newRuleSystem.add(heightRule)
        
        return newRuleSystem
    }
    
    /**
     Create a new `GKEntity` setup for use as a space
     
     - returns: A new `GKEntity` with components and settings suitable for representing a space
     */
    static func newSpace() -> GKEntity
    {
        let newSpaceEntity = GKEntity()
//      Create and associate node
        let newSpaceNode = SpaceNode()
        newSpaceEntity.addComponent(GKSKNodeComponent(node: newSpaceNode))
//      Add components
        newSpaceEntity.addComponent(HoldComponent())
        
        return newSpaceEntity
    }
    
    /**
     Create a new `GKEntity` setup for use as a representation of a tower block
     
     - returns: A new `GKEntity` with components and settings suitable for representing a toewr block
     */
    static func newBlock(withNumberOfRows rows: Int, numberOfColumns columns: Int, matches: Int) -> GKEntity
    {
        let newBlockEntity = GKEntity()
        if let newBlockNode = SKNode(fileNamed: "BlockNode")?.childNode(withName: "block")// as? BlockNode
        {
            newBlockNode.removeFromParent()
            newBlockEntity.addComponent(GKSKNodeComponent(node: newBlockNode))
        }
//      Add components
        let newGridComponent = GridComponent(numberOfColumns: columns, numberOfRows: rows)
        newBlockEntity.addComponent(newGridComponent)
        
        if let columnRange = newGridComponent.grid.ranges?[0],
            let rowRange = newGridComponent.grid.ranges?[1]
        {
            for nextColumn in columnRange
            {
                for nextRow in rowRange
                {
                    let newSpace = Entities.newSpace()
                    newGridComponent.grid[nextColumn, nextRow] = newSpace
                    if let newSpaceNode = newSpace.component(ofType: GKSKNodeComponent.self)?.node
                    {
                        newSpaceNode.name = "space.\(nextColumn).\(nextRow)"
                        newBlockEntity.component(ofType: GKSKNodeComponent.self)?.node.addChild(newSpaceNode)
                    }
                }
            }
        }
        newBlockEntity.addComponent(MatcherComponent())
        let newRuleSystem = self.blockRuleSystemForBlock(newBlockEntity)
        let newRuleSystemComponent = RuleSystemComponent(with: newRuleSystem)
        newBlockEntity.addComponent(newRuleSystemComponent)
        let newGemComponent = GemComponent(count: matches)
        newBlockEntity.addComponent(newGemComponent)
        (newBlockEntity.component(ofType: GKSKNodeComponent.self)?.node as? BlockNode)?.layout()
        
        return newBlockEntity
    }
    
    fileprivate static func blockRuleSystemForBlock(_ block: GKEntity) -> GKRuleSystem
    {
        let blockRuleSystem = GKRuleSystem()
        
//      Creating matching rules
        blockRuleSystem.state["element"] = [String:String]()
        guard let blockGrid = block.component(ofType: GridComponent.self)?.grid else { return blockRuleSystem }
        if let columnRange = blockGrid.ranges?[0],
            let rowRange = blockGrid.ranges?[1]
        {
            for (nextColumnIndex, _) in columnRange.enumerated()
            {
                for (nextRowIndex, _) in rowRange.enumerated()
                {
                    guard let spacePredicate = block.component(ofType: MatcherComponent.self)?.predicateForMatchingSpaceAtColumn(nextColumnIndex, row: nextRowIndex) else { continue }
                    let nextSpaceRule = GKRule(predicate: spacePredicate, assertingFact: "matched.\(nextColumnIndex).\(nextRowIndex)" as NSObjectProtocol, grade: 1.0)
                    nextSpaceRule.salience = 2
                    blockRuleSystem.add(nextSpaceRule)
                }
            }
//          Create filled rules
            var fillPredicates = [NSPredicate]()
            for (nextColumnIndex, _) in columnRange.enumerated()
            {
                for (nextRowIndex, _) in rowRange.enumerated()
                {
                    let filledSpacePredicate = NSPredicate(format: "%K != NIL", "state.element.\(nextColumnIndex)_\(nextRowIndex)")
                    fillPredicates.append(filledSpacePredicate)
                }
            }
            let allFilledPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: fillPredicates)
            let fillRule = GKRule(predicate: allFilledPredicate, assertingFact: "filled" as NSObjectProtocol, grade: 1.0)
            fillRule.salience = 1
            blockRuleSystem.add(fillRule)
        }
        
        return blockRuleSystem
    }
}
