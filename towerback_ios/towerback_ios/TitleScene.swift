//
//  TitleScene.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/12/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import SpriteKit
import GameplayKit

// TODO: Create popup button node for displaying test while touch is active
class TitleScene: SKScene
{
    private let TITLE_SCENE_NODE_NAME_LETTER = "letter"
    
    var gkScene : GKScene?
    var lastUpdateTime : TimeInterval?
    let runeColors : [SKColor] = [#colorLiteral(red: 0.9093305469, green: 0.5507619977, blue: 0.7846211791, alpha: 1),#colorLiteral(red: 0.6722971201, green: 0.7896063328, blue: 1, alpha: 1),#colorLiteral(red: 0.4954623448, green: 0.7896062732, blue: 0.7844479084, alpha: 1),#colorLiteral(red: 1, green: 0.8345418877, blue: 0.5668705663, alpha: 1),#colorLiteral(red: 0.8221204934, green: 1, blue: 0.8012526154, alpha: 1),#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
    var letterSequence : ShuffleSequence<SKNode>?
    
    var playNode : ButtonNode?
    {
        return self.childNode(withName: "//ui.play") as? ButtonNode
    }
    var towerNode : SKNode?
    {
        return self.childNode(withName: "//tower")
    }
    var backgroundEmitterNode : SKEmitterNode?
    {
        return self.childNode(withName: "//emitter.background") as? SKEmitterNode
    }
    
    private func colorRandomBlock()
    {
        guard let nextLetter = self.letterSequence?.next() else { return }
        
        //TODO: Match colors to rest of game
        var nextColor : SKColor
        repeat
        {
            let randomColorIndex = Int(arc4random_uniform(UInt32(self.runeColors.count)))
            nextColor = self.runeColors[randomColorIndex]
        } while nextColor == (nextLetter as? SKLabelNode)?.color
        
        self.changeBlock(nextLetter, toColor: nextColor)
    }
    
    private func changeBlock(_ node: SKNode, toColor color: SKColor)
    {
        let colorAction = SKAction.colorize(with: color, colorBlendFactor: 1.0, duration: 1.45)
        node.run(colorAction, withKey: "color.change")
    }
    
    override func update(_ currentTime: TimeInterval)
    {
        let deltaTime = self.lastUpdateTime != nil ? currentTime - self.lastUpdateTime! : 0
        self.entity?.update(deltaTime: deltaTime)
        self.lastUpdateTime = currentTime
    }
    
    override func didMove(to view: SKView)
    {
        self.backgroundEmitterNode?.advanceSimulationTime(27.7)
        self.isUserInteractionEnabled = true
        
        self.playNode?.isUserInteractionEnabled = true
        self.playNode?.interactionActions[.began] = SKAction.customAction(withDuration: 0.34)
        {
            node, time in
            (node.childNode(withName: "label") as? SKLabelNode)?.color = #colorLiteral(red: 0.3831342757, green: 0.3831342757, blue: 0.3831342757, alpha: 1)
            (node.childNode(withName: "label") as? SKLabelNode)?.colorBlendFactor = time / 0.34
        }
        self.playNode?.interactionActions[.ended] = SKAction.customAction(withDuration: 0.34)
        {
            node, time in
            (node.childNode(withName: "label") as? SKLabelNode)?.colorBlendFactor = 1.0 - time / 0.34
        }
        self.playNode?.action =
        {
            (self.view as? GameSceneView)?.loadTower()

        }
        
//      Create letter sequence
        self.letterSequence = try? ShuffleSequence(withItems: self["\(TITLE_SCENE_NODE_NAME_LETTER).*"])
        
        let coloringComponent = TimerComponent(forInterval: 1.825, repeating: true, started: true)
        {
            _ in
            self.colorRandomBlock()
        }
        let newEntity = GKEntity()
        newEntity.addComponent(GKSKNodeComponent(node: self))
        newEntity.addComponent(coloringComponent)
        self.gkScene?.addEntity(newEntity)
    }
}
