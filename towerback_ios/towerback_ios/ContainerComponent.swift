//
//  ContainerComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/14/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class ContainerComponent: GKComponent
{
    @GKInspectable var count : Int = 3
    @GKInspectable var enemySize : CGSize = CGSize()
    @GKInspectable var startPosition : CGPoint = CGPoint()
}
