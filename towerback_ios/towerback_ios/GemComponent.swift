//
//  GemComponent.swift
//  towerback_ios
//
//  Created by Duncan Oliver on 9/9/16.
//  Copyright © 2016 Highjacks. All rights reserved.
//

import GameplayKit

class GemComponent: GKComponent
{
    fileprivate(set) var values : [Int?]
    var full : Bool
    {
        return !self.values.contains
        {
            return $0 == nil
        }
    }
    
    init(count: Int = 1)
    {
        self.values = [Int?](repeating: nil, count: count)
        
        super.init()
    }

    required init?(coder aDecoder: NSCoder)
    {
        self.values = [Int?]()
        
        super.init(coder: aDecoder)
    }
    
    func add(_ value: Int = 1)
    {
        for nextIndex in 0..<self.values.count
        {
            if self.values[nextIndex] == nil
            {
                self.values[nextIndex] = value
                break
            }
        }
    }
}
